terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.41.0"
    }
  }
}

provider "google" {
  credentials = file(var.credentials_file)

  project = var.project
  region  = var.region
  zone    = var.zone
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}

resource "google_compute_instance" "load_balancer_instance" {
  machine_type = "f1-micro"
  tags         = ["load-balancer"]
  name         = "load-balancer"
  metadata = {
    user-data = file("cloud-config.yaml")
  }

  boot_disk {
    initialize_params {
      image = "ubuntu-1804-bionic-v20230131"
    }
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }
}

resource "google_compute_instance" "web_instance" {
  machine_type = "f1-micro"
  tags         = ["web"]
  count        = 2
  name         = "web-${count.index + 1}"
  metadata = {
    user-data = file("cloud-config.yaml")
  }

  boot_disk {
    initialize_params {
      image = "ubuntu-1804-bionic-v20230131"
    }
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }
}

resource "google_compute_firewall" "rules" {
  name    = "my-firewall-rule"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
}
