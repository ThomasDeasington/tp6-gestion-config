output "web_ip" {
  value = google_compute_instance.web_instance[*].network_interface.0.network_ip
}

output "load_balancer_ip" {
  value = google_compute_instance.load_balancer_instance.network_interface.0.network_ip
}
