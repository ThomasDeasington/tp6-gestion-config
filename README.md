# Projet : Gestion de configuration


## Étape 1: Déployer avec Terraform sur Google cloud :
  - 1 VPC (configure firewall rules)
  - 1 VM load balancer (cloud-init -> installation de git & ansible)
  - 2 VMs application web (cloud-init -> installation de git & ansible)

## Étape 2: Avec Ansible :
  - playbook pour installer et configurer le load balancer
  - playbook pour installer et configurer les VMs application web

## Étape 3: .gitlab-ci.yml :
  - Test des fichiers de configuration avec Ansible Lint
  - Déploiement sur Google cloud avec Terraform
  - Configuration des VMs avec Ansible (inventaire en dur puis inventaire dynamique si le temps)
  - Test de vie
